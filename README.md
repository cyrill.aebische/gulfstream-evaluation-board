# GULFstream Evaluation Board Firmware

## Description
Within this folder are to find all the source code utilised for the project GULFstream Evaluation Board Firmware Development.
There are a few Vivado projects and this documemnt describes what to expect in every location.

--------------------------------------------------------------------------------------------------
Documents: This folder holds documents with supplementary information about the project. In this folder you find the FPGA Pin Mapping.

-- Simulation Files: This folder holds the .dat or .coe files used for simulating data. dat-files are for the  testbench read process and coe-files for the ROM configuration. 

FW_SW_GULF: This is the project ready for use with the GULFstream Evaluation Board.

FW_SW_Test: This project is for testing the firmware with a GULF-simulating PYNQ board.

FW_testbench: This holds the testbench project with a prepared waveform configuration to visualize all the important signals.

gulf_simulation: This is the Vivado project for the GULF simulation. Load the bitstream from this project on the GULF-simulating PYNQ board.

ip_repo: contains the IPs used in the project. When editing IP's take care on editing them in another project and re-pack properly.

Jupyter_Eval_Board: In this folder you find all the Python scripts and the Notebook file GULF_Eval_Board.ipynb .

--------------------------------------------------------------------------------------------------
## Installation
Fork the project to copy everything into a local folder.

For use with the GULFstream Evaluation Board: Generate and load the bitstream from FW_SW_GULF on the evaluation PYNQ board. 

For use with the Simulation PYNQ board: Generate and load the bitstream from FW_SW_Test on the evaluation PYNQ board, and the bitstream from gulf_simulation on the simulating PYNQ board.

--------------------------------------------------------------------------------------------------
## Usage
Functions of the PYNQ buttons are:

Evaluation PYNQ:
btn1 = reset all registers and counters

GULF-simulation PYNQ:
btn0 = start data transmission
btn1 = reset all registers and counters

